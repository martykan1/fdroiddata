Categories:Internet,System
License:GPLv3
Web Site:
Source Code:https://github.com/andDevW/getChromium
Issue Tracker:https://github.com/andDevW/getChromium/issues
Bitcoin:188RxvRnSXSZZnjuDdLwNirHDfNusVPobh

Auto Name:getChromium
Summary:Installs/Updates the open-source Chromium browser
Description:
Companion app that downloads and installs Chromium updates.
.

Repo Type:git
Repo:https://github.com/andDevW/getChromium

Build:1.0,1
    commit=bacc6f79fbed5d599e7cfd164b9691e50e2b6a70
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.11" }' >> ../build.gradle
    gradle=yes
    rm=app/libs/*

Build:2.1,20160614
    commit=913065436c6f6c8270b61f7b8fbbfa0688c8f493
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.11" }' >> ../build.gradle
    gradle=yes
    rm=app/libs/*

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.0
Current Version Code:2
