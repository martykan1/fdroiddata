AntiFeatures:NonFreeNet
Categories:System
License:GPLv3
Web Site:https://github.com/rumboalla/apkupdater/blob/HEAD/README.md
Source Code:https://github.com/rumboalla/apkupdater
Issue Tracker:https://github.com/rumboalla/apkupdater/issues

Auto Name:APKUpdater
Summary:Download APKs
Description:
Download APKs from Evozi (GooglePlay) and other sources, e.g. APKPure or
APKMirror.
.

Repo Type:git
Repo:https://github.com/rumboalla/apkupdater
Binaries:https://github.com/rumboalla/apkupdater/releases/download/%v/app-release.apk

Build:1.1.1,1
    disable=does not validate
    commit=1.1.1
    subdir=app
    gradle=yes

Maintainer Notes:
Builds, but does not validate against upstream.
Versioncode does not get bumped, see https://github.com/rumboalla/apkupdater/issues/5.
Newer versions do not compile.
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.1.1
Current Version Code:1
